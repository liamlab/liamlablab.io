$(function(){
	energyGnb();
	visualSlide();
	contSlide();
	mBoard();
	pzSlide();
	mSitemap();
	familySite();
	searchDetail();
	match();
});


/*--------------------------------------------------*/
/* energy Gnb
/*--------------------------------------------------*/
function energyGnb(){

	// variables
	$html = $('html');
	$header = $('#header');
	$headerTop = $('.headerTop');
	$headerNav = $('.headerNav');
	$gnb = $('#gnb');
	$modalOverlay = $('.modalOverlay');
	$menuBtn = $('.menuBtn');
	$hamburger = $('.hamburger');
	$searchBtn = $('.searchBtn');
	$searchBar = $('.searchBar');
	$searchClose = $('.searchClose');

	//menu button animation
	function menuOpenBtn() {
		$menuBtn.find('.srOnly').html('전체메뉴닫기');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:0, width:30, height:3, rotation:-45, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {opacity:0, width:0, height:3, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line3'), .2, {marginTop:0, width:30, height:3, rotation:45, ease:Circ.easeOut});
	};
	function menuCloseBtn() {
		$menuBtn.find('.srOnly').html('전체메뉴보기');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:-10, width:28, height:3, rotation:0, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {opacity:1, width:28, height:3, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line3'), .2, {marginTop:10, width:28, height:3, rotation:0, ease:Circ.easeOut});
	};

	// gnb motion
	function gnbOpen() {
		var gnbH = $gnb.height();
		if($header.is('.gnbWeb')){
			TweenMax.to($headerNav, .3, {height:gnbH+1, ease:Circ.easeOut});
		};
	};
	function gnbClose() {
		if($header.is('.gnbWeb')){
			TweenMax.to($headerNav, .3, {height:55, ease:Circ.easeOut});
		};
		if($header.is('.gnbMobile')){
			TweenMax.to($headerNav, .3, {height:'auto', ease:Circ.easeOut});
		};
	};

	// menu button click motion
	$menuBtn.on('click', function() {
		$(this).toggleClass('close');
		if (matchMedia('screen and (max-width: 1024px)').matches) {
			if($(this).is('.close')){
				$header.addClass('mGnbOpen');
				$headerNav.prepend('<p class="mNavTitle">전체메뉴</p>');
				$html.css('overflow-y', 'hidden');
				menuOpenBtn();
			} else {
				$header.removeClass('mGnbOpen');
				$headerNav.find('.mNavTitle').remove();
				$html.removeAttr('style');
				menuCloseBtn();
			};
		} else {
			if($(this).is('.close')){
				$header.addClass('allMenu');
				$html.css('overflow-y', 'hidden');
				menuOpenBtn();
				gnbOpen();
				$("#gnb>ul>li:first-child>a").focus();
			} else {
				$header.removeClass('allMenu');
				$html.removeAttr('style');
				menuCloseBtn();
				gnbClose();
			};
		};
	});

	// $search button click motion
	$searchBtn.on('click', function() {
		$(this).addClass('isOpen');
		$(".searchBar input").focus();
	});
	$searchClose.on('click', function() {
		$searchBtn.removeClass('isOpen');
		$searchBtn.focus();
	});

	// web gnb motion
	$gnb.delegate('.depth1 a', 'mouseenter focusin click', function (event) {
		var $this = $(this);
		switch (event.type) {
			case 'mouseenter' :
			case 'focusin' :
				if($header.is('.gnbWeb')){
					gnbOpen();
				};
			break;
			case 'click' :
				if($header.is('.gnbMobile')){
					if($(this).parent().is('.isOpen')) {
						$(this).parent().removeClass('isOpen');
						$(this).parent().find('.depth2').slideUp(200);
					} else {
						$gnb.find('.depth1 > li').removeClass('isOpen');
						$(this).parent().addClass('isOpen');
						$gnb.find('.depth2').slideUp();
						$(this).parent().find('.depth2').slideDown(200);
					};
					if($(this).next().is('.depth2')) {
						event.preventDefault();
					}
				};
			break;
		}
	});
	$header.on('mouseleave',function(){
		if(!$menuBtn.is('.close')){
			gnbClose();
		}
	});

	// FOCUS
	$menuBtn.on('focusin', function() {
		if($header.is('.gnbWeb')){
			if(!$menuBtn.is('.close')){
				gnbClose();
			}
		};
	});
	$menuBtn.on('focusout', function() {
		if($header.is('.gnbWeb')){
			if($menuBtn.is('.close')){
				$("#gnb>ul>li:first-child>a").focus();
			}
		};
	});
	$searchClose.on('focusout', function() {
		$(".searchBar input").focus();
	});

	// load resize effect
	$(window).on('load resize', function(){
		if (matchMedia('screen and (max-width: 1024px)').matches) {
			$header.removeClass().addClass('gnbMobile');
		} else {
			$header.removeClass().addClass('gnbWeb');
			$gnb.find('.depth1 > li').removeClass();
			$gnb.find('.depth2').removeAttr('style');
			$headerNav.find('.mNavTitle').remove();
		}
		$html.removeAttr('style');
		$headerNav.removeAttr('style');
		$menuBtn.removeClass('close');
		menuCloseBtn();
		gnbClose();
	});

}


/*--------------------------------------------------*/
/*  visualSlide
/*--------------------------------------------------*/
function visualSlide() {

	var $slideVisual = $(".visualSlide");

	$slideVisual.on('init',function (event, slick, currentSlide) {
		$('.slick-arrow').wrapInner('<span class="srOnly"></span>');
		$('.slick-dots').wrap('<div class="slideControl"></div>');
		$('.slideControl').append('<button class="slidePlay pause"><span class="srOnly">슬라이드 정지</span></button>');
		$('#siteLink').addClass('slide1');
		$('.slick-prev').find('.srOnly').text('이전 슬라이드');
		$('.slick-next').find('.srOnly').text('다음 슬라이드');
		// $('.slick-slide').attr({'tabindex':'-1'});
		// $('.slick-slide.slick-active').removeAttr("tabindex");
	});

	// $slideVisual.on('beforeChange',function (event, slick, currentSlide, nextSlide) {
	// 	var i = (nextSlide ? nextSlide : 0) + 1;
	// 	$('#siteLink').removeClass().addClass('slide' + i);
	// });

	// $slideVisual.on('afterChange',function (event, slick, currentSlide, nextSlide) {
	// 	$('.slick-slide').attr({'tabindex':'-1'});
	// 	$('.slick-slide.slick-active').removeAttr("tabindex");
	// });

	$slideVisual.slick({
		dots: true,
		arrows: true,
		infinite: true,
		fade: false,
		cssEase: 'linear',
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		speed: 600,
		pauseOnHover: false,
		accessibility: false
	});

	$('.slidePlay').on('click', function(e) {
		$(this).toggleClass('pause');
		if($(this).is(".pause")) {
			$(this).find('.srOnly').html('슬라이드 정지');
			$slideVisual.slick('slickPlay');
		} else {
			$(this).find('.srOnly').html('슬라이드 재생');
			$slideVisual.slick('slickPause');
		}
		e.preventDefault();
	});

}


/*--------------------------------------------------*/
/*  content Slide
/*--------------------------------------------------*/
function contSlide() {

	var $slideCont = $(".contSlide");

	$slideCont.on('init',function (event, slick, currentSlide) {
		$('.slick-slide, .slick-slide>a').attr({'tabindex':'-1'});
		$('.slick-slide.slick-active, .slick-slide.slick-active>a').removeAttr("tabindex");
	});

	$slideCont.on('afterChange',function (event, slick, currentSlide, nextSlide) {
		$('.slick-slide, .slick-slide>a').attr({'tabindex':'-1'});
		$('.slick-slide.slick-active, .slick-slide.slick-active>a').removeAttr("tabindex");
	});

	$slideCont.slick({
		dots: false,
		prevArrow: $('.contSlideWrap').find('.slick-prev'),
		nextArrow: $('.contSlideWrap').find('.slick-next'),
		slidesToShow:4,
		slidesToScroll:1,
		autoplay:false,
		autoplaySpeed:5000,
		speed: 300,
		accessibility: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});

}


/*--------------------------------------------------*/
/*  main Board
/*--------------------------------------------------*/
function mBoard() {

	$mBoardTitle = $('.mainBoard h3');
	$mBoardTab = $('.mainBoard h3 a');

	$('.mNotice').addClass('isActive');

	$mBoardTab.on('click focus', function(e) {
		$mBoardTitle.removeClass('isActive');
		$(this).parent().addClass('isActive');
	});
}


/*--------------------------------------------------*/
/*  popupZone Slide
/*--------------------------------------------------*/
function pzSlide() {

	var $pzSlide = $(".pzList"),
			$currentNum = $('.pzState .currentNum'),
			$totalNum = $('.pzState .totalNum');

	$pzSlide.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
		var i = (currentSlide ? currentSlide : 0) + 1;
		$currentNum.text(i);
		$totalNum.text(slick.slideCount);
	});

	$('.pzPlay').on('click', function(e) {
		$(this).toggleClass('pause');
		if($(this).is(".pause")) {
			$(this).find('.srOnly').html('슬라이드 정지');
			$pzSlide.slick('slickPlay');
		} else {
			$(this).find('.srOnly').html('슬라이드 재생');
			$pzSlide.slick('slickPause');
		}
		e.preventDefault();
	});

	$('.pzPrev').on('click', function() {
		$pzSlide.slick('slickPrev');
	});

	$('.pzNext').on('click', function() {
		$pzSlide.slick('slickNext');
	});

	$pzSlide.slick({
		dots:false,
		arrows:false,
		slidesToShow:1,
		slidesToScroll:1,
		autoplay:true,
		//fade:true,
		autoplaySpeed:6000,
		speed: 600,
		pauseOnHover:true,
		accessibility: false
	});

}


/*--------------------------------------------------*/
/*  main Sitemap
/*--------------------------------------------------*/
function mSitemap() {

	$mSitemap = $('.mSitemap');
	$sitemapList = $mSitemap.find('.depth1 > li > a');

	$sitemapList.on('click', function(e) {
		if (matchMedia('screen and (max-width: 767px)').matches) {
			if ($(this).next().is(".depth2")) {
				if ($(this).parent().is(".on")) {
					$(this).parent().removeClass('on');
					$(this).next('ul').slideUp(200);
				} else {
					$sitemapList.parent().removeClass('on');
					$sitemapList.next('ul').slideUp(200);
					$(this).parent().addClass('on');
					$(this).next('ul').slideDown(200);
				}
				e.preventDefault();
			}
		}
	});

	// load resize effect
	$(window).on('load resize', function(){
		if (matchMedia('screen and (min-width: 768px)').matches) {
			$mSitemap.find('.depth2').removeAttr('style');
		}
	});
}


/*--------------------------------------------------*/
/*  familySite
/*--------------------------------------------------*/
function familySite() {

	$familySite = $('.familySite');
	$familyOpen = $('.familyOpen');
	$familyClose = $('.familyClose');

	$familyOpen.on('click', function(e) {
		$(this).parent().toggleClass('isOpen');
		if($(this).parent().is('.isOpen')){
			$(this).next('div').slideDown(200);
		} else {
			$(this).next('div').slideUp(200);
		}
		e.preventDefault();
	});

	$familyClose.on('click', function() {
		$(this).parents('.familySite').removeClass('isOpen');
		$(this).parent().slideUp(200);
	});

	$familySite.on('mouseleave',function(){
		if($(this).is('isOpen')){
			$(this).removeClass('isOpen');
			$(this).find('div').slideUp(200);
		}
	});

}


/*--------------------------------------------------*/
/*  search detail
/*--------------------------------------------------*/
function searchDetail() {

	$searchDetail = $('.searchDetail');
	$detailBtn = $('.detailBtn');

	$detailBtn.on('click', function(e) {
		$(this).toggleClass('isOpen');

		if($(this).is('.isOpen')){
			$(this).find('span').html('고급검색 닫기');
			$searchDetail.slideDown(300);
			setTimeout(function() {
				$searchDetail.find('.fieldWrap:first-child > .chkField > span:first-child > input').focus();
			}, 300);
		} else {
			$(this).find('span').html('고급검색 열기');
			$searchDetail.slideUp(300);
		}
		e.preventDefault();
	});

}


/*--------------------------------------------------*/
/*  matchHeight
/*--------------------------------------------------*/
function match() {

	$(window).on('load resize', function(){

		$('.gridList').each(function() {
			$(this).children('li').matchHeight();
		});

		$('.trafficInfo.bus ul').each(function() {
			$(this).children('li').matchHeight();
		});

	});

}

/*--------------------------------------------------*/
/*  recommend
/*--------------------------------------------------*/
function recommend(nttId) {
	$.post(
		"/energy/singl/recommend/doLike.json",
		{nttId : nttId},
		function(data) {
			var resultCode = data.resultCode;
			if (resultCode == "success") {
				$('.like').text(data.cnt);
			}
			else {
				alert("이미 추천 하셨습니다.");
			}
		},"json"
	)
}

