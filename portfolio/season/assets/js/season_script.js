$(function(){
	seasonIntro();
	seasonCommon();
	//mainVisual();
	menuItem();
	flipBox();
	matchH();
});


/*--------------------------------------------------*/
/*  season intro
/*--------------------------------------------------*/
function seasonIntro(){

	// variables
	$html = $('html');
	$body = $('body');
	$loadingWrap = $('.loadingWrap');
	$loadingLogo = $loadingWrap.find('.loadingLogo');
	$loadingDim = $loadingWrap.find('.loadingDim');
	$videoWrap = $('.videoWrap');
	$videoBox = $videoWrap.find('.videoBox');
	$videoDim = $videoWrap.find('.videoDim');

	if($body.is('.intro')){

		$html.css('overflow-y', 'hidden');

		setTimeout(function(){
			new TimelineMax()
				.to($loadingLogo, .5, {opacity:0,  ease:Sine.easeOut})
				.to($loadingDim, 1, {opacity:0,  ease:Sine.easeOut})
				.to($loadingWrap, 0, {display:'none'});
			
			$("#seasonVideo").get(0).play(); //동영상 재생
			
		}, 2000);

		$('.videoDim, .videoClose').click(function() {
			
			new TimelineMax()
				.to($videoBox, .5, {opacity:0,  ease:Sine.easeOut})
				.to($videoDim, 1, {opacity:0,  ease:Sine.easeOut})
				.to($videoWrap, 0, {display:'none'});
			$html.removeAttr('style');

			$("#seasonVideo").stop(); //동영상 멈춤
			$("#seasonVideo").remove();
		});

	}

}

/*--------------------------------------------------*/
/*  season Common
/*--------------------------------------------------*/
function seasonCommon(){
	
	// variables
	$html = $('html');
	$body = $('body');
	$header = $('#header');
	$gnb = $('#gnb');
	$menuBtn = $('.menuBtn');
	$hamburger = $('.hamburger');
	$gnbDim = $('.gnbDim');
	$moveTopBtn = $('.btnTop');

	// menu button animation
	function menuOpenBtn() {
		$menuBtn.find('.tit').html('CLOSE');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:0, height:3, rotation:-45, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {marginTop:0, height:3, rotation:45, ease:Circ.easeOut});
	};
	function menuCloseBtn() {
		$menuBtn.find('.tit').html('MENU');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:-5, height:4, rotation:0, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {marginTop:5, height:4, rotation:0, ease:Circ.easeOut});
	};

	// menu button click motion
	$menuBtn.on('click', function() {
		$header.toggleClass('isActive');
		if($header.is('.isActive')){
			menuOpenBtn();
			$gnb.slideDown(250);
			$gnbDim.show(250);
		} else {
			menuCloseBtn();
			$gnb.slideUp(250);
			$gnbDim.hide(250);
		};
	});

	// move top
	$moveTopBtn.click(function(){
		$html.animate({scrollTop: 0}, 400);
		$body.animate({scrollTop: 0}, 400);
		return false;
	});

	// layer open/close
	$(".layerOpen").click(function () {

		var winH = $(window).height(),
				layerH = $(".layerWrap").height(),
				layerContH = winH - 70,
				getPopupID = $(this).attr("name");

		if($header.is('.isActive')){
			$header.removeClass('isActive');
			menuCloseBtn();
			$gnb.slideUp(250);
			$gnbDim.hide(250);
		}
		if(winH <= layerH) {
			$('.layerContent').css('height', layerContH);
		}
		$('#'+getPopupID).show();
		$('.layerDim').show(250);
		$html.css('overflow-y', 'hidden');
	});

	$('.layerClose, .layerDim').click(function() {
		$('.layerWrap').hide();
		$('.layerDim').hide(250);
		$html.removeAttr('style');
	})

	// load resize effect
	$(window).on('load resize', function(){

		
	});
	
}


/*--------------------------------------------------*/
/*  main visual area
/*--------------------------------------------------*/
function mainVisual() {

	function visualObject() {
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item1'), 1.2, {delay:.5, rotation:-3,  ease:Sine.easeOut})
			.to($('.visualObject .item1'), 1.2, {rotation:3, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item2'), 1.1, {delay:.3, rotation:-2, ease:Sine.easeOut})
			.to($('.visualObject .item2'), 1.1, {rotation:2, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item3'), .8, {delay:1, rotation:-1, ease:Sine.easeOut})
			.to($('.visualObject .item3'), .8, {rotation:1, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item4'), 1, {delay:.3, rotation:-2, ease:Sine.easeOut})
			.to($('.visualObject .item4'), 1, {rotation:2, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item5'), .5, {delay:.7, rotation:-3, ease:Sine.easeOut})
			.to($('.visualObject .item5'), .5, {rotation:3, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item6'), 1.2, {delay:.5, rotation:-3, ease:Sine.easeOut})
			.to($('.visualObject .item6'), 1.2, {rotation:3, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item7'), 1.1, {delay:.3, rotation:-2, ease:Sine.easeOut})
			.to($('.visualObject .item7'), 1.1, {rotation:2, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item8'), .8, {delay:1, rotation:-1, ease:Sine.easeOut})
			.to($('.visualObject .item8'), .8, {rotation:1, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item9'), 1, {delay:.3, rotation:-2, ease:Sine.easeOut})
			.to($('.visualObject .item9'), 1, {rotation:2, ease:Sine.easeIn});
		new TimelineMax({repeat:-1, repeatDelay:0, yoyo:true})
			.to($('.visualObject .item10'), 1.5, {delay:.7, rotation:-3, ease:Sine.easeOut})
			.to($('.visualObject .item10'), 1.5, {rotation:3, ease:Sine.easeIn});
	}
	visualObject();

	function parallaxEf() {
		//parallax.js 
		var scene = $('#scene').get(0);
		var parallaxInstance = new Parallax(scene, {
			limitY:0, 
		});
	}
	parallaxEf();

	// load resize effect
	$(window).on('load resize', function(){

		var winW = $(window).width();

		if (matchMedia('screen and (min-width: 1281px)').matches) {
			$('.visualObject').parent().attr("data-depth","3.6");
		}
		if (matchMedia('screen and (max-width: 1280px)').matches) {
			$('.visualObject').parent().attr("data-depth","7");
		}
		if (matchMedia('screen and (max-width: 1024px)').matches) {
			$('.visualObject').parent().attr("data-depth","9");
		}
		if (matchMedia('screen and (max-width: 640px)').matches) {
			$('.visualObject').parent().attr("data-depth","12");
		}
		if (matchMedia('screen and (max-width: 500px)').matches) {
			$('.visualObject').parent().attr("data-depth","16");
		}

	});

}


/*--------------------------------------------------*/
/*  menu item easing effect
/*--------------------------------------------------*/
function menuItem() {

	var dragArea = $('.dragArea');

	dragArea.each(function() {

		$(this).on('mouseleave', function(e) {
			TweenMax.to($(this).find('> *'), 1.8, {
				x: 0, 
				y: 0, 
				ease: Elastic.easeOut,
			});
		});

		$(this).on('mousemove', function(e) {
			var target = $(this).find('> *');
			var movementX = $(this).width() / 2;
			var movementY = $(this).height() / 3;
			var relX = e.pageX - $(this).offset().left;
			var relY = e.pageY - $(this).offset().top;
			TweenMax.to(target, 1, {
				x: (relX - $(this).width() / 2) / $(this).width() * movementX,
				y: (relY - $(this).height() / 2) / $(this).height() * movementY,
				ease: Power2.easeOut,
			});
		});

	});

}


/*--------------------------------------------------*/
/*  Flip box effect
/*--------------------------------------------------*/
function flipBox() {

	var flipBox = $('.flipBox'),
			boxWid = flipBox.width();

	flipBox.each(function() {

		$(this).on('mouseover', function(e) {
			TweenMax.to($(this).find('.unit'), .5, {rotateY:180, ease: Power2.easeOut});
			TweenMax.to($(this).find('.front'), .5, {opacity:0, rotateY:-180, ease: Power2.easeOut});
			TweenMax.to($(this).find('.back'), .5, {opacity:1, rotateY:-180, ease: Power2.easeOut});
		});

		$(this).on('mouseleave', function(e) {
			TweenMax.to($(this).find('.unit'), .5, {rotateY:0, ease: Power2.easeOut});
			TweenMax.to($(this).find('.front'), .5, {opacity:1, rotateY:0, ease: Power2.easeOut});
			TweenMax.to($(this).find('.back'), .5, {opacity:0, rotateY:0, ease: Power2.easeOut});
		});

	});

}


/*--------------------------------------------------*/
/*  matchHeight
/*--------------------------------------------------*/
function matchH() {

	$(window).on('load resize', function(){

		$('.typeCont').each(function() {
			$(this).children('li').matchHeight();
		});
		$('.performanceList').each(function() {
			$(this).children('li').matchHeight();
		});

	});
	
}