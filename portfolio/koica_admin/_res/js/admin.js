$(document).ready(function(){

	// layer open/close
	$(".layer-open").click(function () {
		var getPopupID = $(this).attr("name");
		$('#'+getPopupID).show().addClass("isOpen");
		if($('.layer-dim').is(".isOpen")) {
			var getPopH = $('#'+getPopupID+' .popup-wrap').height(),
					getPopPos = Math.round(-((getPopH+40)*1/2));
			$('#'+getPopupID+' .popup-wrap').css('margin-top', getPopPos);
		}
		$('html').css('overflow-y', 'hidden');
	});
	$('.layer-close').click(function() {
		$(this).parents('.layer-dim').hide().removeClass("isOpen");
		if($('.layer-dim').is(".isOpen")) {
			$('html').css('overflow-y', 'hidden');
		} else {
			$('html').removeAttr('style');
			//$(this).parents('.popup-wrap').removeAttr('style');
			$(this).parents('.popup-wrap').css('margin-top', 'auto'); /* 200514 */
		}
	});

	// search-filter open/close
	$(".filter-open").click(function () {
		$('.search-filter').show();
	});
	$('.filter-close').click(function() {
		$('.search-filter').hide();
	});

	// tab content show/hide
	function defaultTab() {
		// $('.c-tab.in-cont li:first-child').addClass('over');
		// $('.tab-cont').hide();
		// $('.tab-cont-wrap > .tab-cont:first-child').show();
		if($('.popup-contents').hasClass("lastOpen")) {
			$('.c-tab.in-cont li:last-child').addClass('over');
			$('.tab-cont').hide();
			$('.tab-cont-wrap > .tab-cont:last-child').show();
		} else {
			$('.c-tab.in-cont li:first-child').addClass('over');
			$('.tab-cont').hide();
			$('.tab-cont-wrap > .tab-cont:first-child').show();
		}
	} 
	defaultTab();
	$('.c-tab.in-cont a').click(function(){
		var getTabID = $(this).attr('name');
		$('.tab-cont').hide();
		$('#'+getTabID).show();
		$('.c-tab.in-cont li').removeClass('over');
		$(this).parent('li').addClass('over');
	});
	$('.c-tab a').click(function(){
		$('.c-tab li').removeClass('over');
		$(this).parent('li').addClass('over');
	});

	function filterTab() {
		$('.f-tab.in-cont li:first-child').addClass('over');
		$('.f-tab-cont').hide();
		$('.f-tab-cont-wrap > .f-tab-cont:first-child').show();
	} 
	filterTab();
	$('.f-tab.in-cont a').click(function(){
		var getTabID = $(this).attr('name');
		$('.f-tab-cont').hide();
		$('#'+getTabID).show();
		$('.f-tab.in-cont li').removeClass('over');
		$(this).parent('li').addClass('over');
	});

	function subTab() {
		$('.s-tab.in-cont li:first-child').addClass('over');
		$('.s-tab-cont').hide();
		$('.s-tab-cont-wrap > .s-tab-cont:first-child').show();
	} 
	subTab();
	$('.s-tab.in-cont a').click(function(){
		var getTabID = $(this).attr('name');
		$('.s-tab-cont').hide();
		$('#'+getTabID).show();
		$('.s-tab.in-cont li').removeClass('over');
		$(this).parent('li').addClass('over');
	});

});

$(window).on('load resize', function() {

	// leftMenuH
	var leftMenuH = $(window).height()-185;
	$('#leftmenu').css({'height':leftMenuH});

	// matchItem
	var matchItem = $('.gallery-list, .condition-list');
	matchItem.each(function() {
		$(this).children('li').matchHeight();
	});

	// faq-list open/close
	$(".faq-list dt .q-btn").on('click', function(e) {
		$(this).parent().toggleClass('isOpen');
		if($(this).parent().hasClass("isOpen")) {
			$(this).parent().next('dd').show();
			$(this).find('span.blind').text('닫기');
		} else {
			$(this).parent().next('dd').hide();
			$(this).find('span.blind').text('열기');
		}
		e.preventDefault();
	});


});

$(function(){
	$(".datepicker" ).datepicker({
		changeYear: true
		, changeMonth: true
		, showMonthAfterYear:true
		, monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12']
		, dateFormat: "yy.mm.dd"
		, dayNames: [ "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일" ]
		, dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ]
		, minDate:new Date()
		, showButtonPanel:true
		, currentText:"오늘 날짜"
		, closeText:'닫기'
		, onClose: function(selectedDate){
			if(selectedDate!=''){
				$("#expsrEndDtd" ).datepicker( "option", "minDate", selectedDate);
			}
		}
	});
});