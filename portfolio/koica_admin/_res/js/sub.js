

$(function(){
	// leftmenu 
	$('.snb-ctrl').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#leftmenu').removeClass('active');
		}else{
			$(this).addClass('active');
			$('#leftmenu').addClass('active');
		}
	});
	$('#leftmenu').find('.lm-l1').each(function(){
		if($(this).find("ul").length > 0){
			$(this).addClass('has-sub');
		}else{
		  
		}
	});

	$('.calendar').find('td').each(function(){
		if($(this).find("ul").length > 0){
			$(this).addClass('shcedule');
		}else{
		  
		}
	});

	$('#snbNav').find('li > a').click(function(){
		$(this).parent().addClass('active');
        $(this).parent().siblings('li').removeClass('active');
	});

	$('.like').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active')
		}
	});
	$('.scrap').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});


	$('.filter').find('.detail-btn').click(function(){
		if($(this).parent().hasClass('active')){
			$(this).parent().removeClass('active');
		}else{
			$(this).parent().addClass('active')
		}
	});

	$('.list01.sel').find('li > div > a').click(function(){
		if($(this).closest('li').hasClass('active')){
			$(this).closest('li').removeClass('active');
		}else{
			$(this).closest('li').addClass('active')
		}
	});
	$('.list04.sel').find('li > div > a').click(function(){
		$(this).closest('li').addClass('active');
		$(this).closest('li').siblings('li').removeClass('active');
	});

	$('.gender-sel').find('li > a').click(function(){
		$(this).parent('li').addClass('active');
		$(this).parent('li').siblings('li').removeClass('active');
	});

	
	$('.plan-tab').find('li > a').click(function(){
		$(this).parent('li').addClass('active');
		$(this).parent('li').siblings('li').removeClass('active');
	});
	/*var $cate  = $('.cate-slide');
	var $catewrap = $cate.parent();
	$cate.sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		//startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 600,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
		prevPage: $catewrap.closest('.ov').find('.prev'),
        nextPage: $catewrap.closest('.ov').find('.next'),
	});*/
	// course-slide
	 

	 var $vcateSlide = $('.vcate-slide');
	 var $vcatewrap = $vcateSlide.parent();
	 $vcateSlide.owlCarousel({
		items: 9,
		autoHeight:false,
		loop: true,
		margin: 0,
		mouseDrag: false,
		touchDrag: false,
		nav:false,
		autoplay: false,
		dots:false,
		responsiveRefreshRate: 1,
		autoplayTimeout: 3500,
		autoplayHoverPause: false,
		responsive:{
			0:{
				items:9
			},
			600:{
				items:9
			},
			1281:{
				items:9,
			}
		}
	 });
	 $vcatewrap.find('.next').click(function() {
		$vcateSlide.trigger('next.owl.carousel', [600]);
	});
	$vcatewrap.find('.prev').click(function() {
		$vcateSlide.trigger('prev.owl.carousel', [600]);
	});



	$('.road-tab').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});

	$('.plan-tab').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		//startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});

	$('.event-slide').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});
	$('.sch-provide').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});
	$('.sch-expert').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});
	$('.sch-network').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});

	/*$('.snb-slide').sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 500,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
	});*/

	if($(".rspvn-tit").length<1) $("<p class='rspvn-tit'><span class='prev'>&nbsp;</span><span class='next'>&nbsp;</span></p>").prependTo($(".rspvn-box")).show();
});

function sizeControlSub(width) {
	width = parseInt(width);
	var bodyHeight = document.documentElement.clientHeight; 
	var bodyWidth = document.documentElement.clientWidth; 
	
	/*var contW = $('#contents').outerWidth();
	var cateWidth= $('.cate-slide').outerWidth();
	if(bodyWidth > 1000){
		$('.cate-slide').find('ul > li').width(cateWidth * 12.5/100 +'px'); 
	}
	$('.cate-slide').sly('reload');*/

	$('.road-tab').sly('reload');
	$('.plan-tab').sly('reload');
	$('.event-slide').sly('reload');
	$('.sch-provide').sly('reload');
	$('.snb-slide').sly('reload');
	$('.sch-network').sly('reload');
	$('.sch-expert').sly('reload');
}
jQuery(function($){
	sizeControlSub($(this).width());
	$(window).resize(function() {
		if(this.resizeTO) {
			clearTimeout(this.resizeTO);
		}
		this.resizeTO = setTimeout(function() {
			$(this).trigger('resizeEnd');
		}, 10);
	});
});	
$(window).on('resizeEnd', function() {
	sizeControlSub($(this).width());
});
$(window).load( function() { 
	sizeControlSub($(this).width());
});
