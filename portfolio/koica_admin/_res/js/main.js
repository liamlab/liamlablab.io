function clearText(thefield) {
  if (thefield.defaultValue==thefield.value) {
    thefield.value="";
  }
} 
function defaultText(thefield) {
  if (thefield.value=="") {
    thefield.value=thefield.defaultValue;
  }
} 
$(document).ready(function(){
	$('body').removeClass().addClass('page-main');
	$("#input-pass").focus(function(){
		$(this).attr("type", "password");
	});
	
	var proListsel = false;
	$('.project-info > ul').find('li > a.isTab').click(function(){
	   if (proListsel) {
		   $(this).clearQueue();
		   $(this).parent('li').find('div.list-wrap').fadeOut();	
		   $(this).parent('li').removeClass('isOver');
		   proListsel = false;
	   } else {
		   $(this).clearQueue();
		   $(this).parent('li').find('div.list-wrap').fadeIn();	
		   $(this).parent('li').removeClass().addClass('isOver');
		   proListsel = true;
	   }
	});
	
	 $('ul.project-list > li').on('click',function(index){
		 $(this).parent('ul').parent('div.list-wrap').fadeOut();	
		 $(this).parent('ul').parent('div.list-wrap').parent('li').removeClass().addClass('isOver');
		 var clickText = $(this).text();
		 $('ul.project-list > li').each(function(index) {
			   $('.project-info').find('.isTab').text(clickText)
		 });
		 proListsel = false;
	 });
	
	$('.login-info').find('.login-btn').on('click',function(){
	   console.log("로그인");
	   $('#login-wrap').addClass('has-project');
	   $('.project-info').stop().animate({'z-index':'1111'})
	   setTimeout(function() {
		  loginhasText();
	   }, 700);
	});
    function loginhasText(){ $(".login-top").find('.stxt').text('찾으시는 홈페이지를 선택해주세요.')}
    var userInputId = getCookie("userInputId");
    
     
    if($("input[name='id']").val() != ""){ 
        $("#id-save").attr("checked", true); 
    }
     
    $("#id-save").change(function(){ 
        if($("#id-save").is(":checked")){ 
            var userInputId = $("input[name='id']").val();
            setCookie("userInputId", userInputId, 30); 
        }else{ 
            deleteCookie("userInputId");
        }
    });
    $("input[name='id']").keyup(function(){ 
        if($("#id-save").is(":checked")){ 
            var userInputId = $("input[name='id']").val();
            setCookie("userInputId", userInputId, 30); 
        }
    });
	
});



function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}
 
function deleteCookie(cookieName){
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}
 
function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

