$(function(){
	common();
	gnb();
	main();
	match();
	network();
	enquirySel();
});

/* common */
function common(){
	$('.familyBtn').on('click', function(e) {
		$('.familySite').toggleClass('familyOpen');
		if($('.familySite').is(".familyOpen")){
			$('.familyList').slideDown();
		} else {
			$('.familyList').slideUp();
		}
		e.preventDefault();
	});
	$('.familySite').on('mouseleave',function(){
		if($('.familySite').is(".familyOpen")){
			$('.familySite').removeClass('familyOpen');
			$('.familyList').slideUp();
		}
	});
	$('.btnTop').click(function(){
		$('body,html').animate({scrollTop: 0}, 600);
		return false;
	});

	function goTopBtn() {
		position = $(window).scrollTop();
		windowH = $(window).height();
		mainVisualH = $('.mainVisual').height();
		containerH = $('#container').height();
		footerH = $('#footer').height();
		btnTopH = $('.btnTop').height();
		contentH = containerH - windowH;
		btnTopPos = footerH - btnTopH;
		if (position > mainVisualH){
			$('.btnTop').fadeIn(150);
		} else {
			$('.btnTop').fadeOut(150);
		}
		if (matchMedia("screen and (max-width: 640px)").matches) {
			$('.btnTop').css({'bottom':btnTopPos - 12});
		} else {
			if (position < contentH){
				$('.btnTop').addClass('btnTopFixed');
				$('.btnTop').css({'bottom':'30px'});
			} else {
				$('.btnTop').removeClass('btnTopFixed');
				$('.btnTop').css({'bottom':btnTopPos - 25});
			}
		}
	}

	$(window).scroll(function(){
		position = $(window).scrollTop();
		if (position > 0){
			$('#header').addClass('gnbFixed');
		} else {
			$('#header').removeClass('gnbFixed');
		}
		goTopBtn();
	});

	$(window).on('load resize', function() {
		$.each($('.changeImg').find('img'), function() {
			if (matchMedia("screen and (max-width: 640px)").matches) {
				var imgSrc = $(this).attr("src").replace('_pc', '_mo');
			} else {
				var imgSrc = $(this).attr("src").replace('_mo', '_pc');
			}
			$(this).attr("src", imgSrc);
		});
		goTopBtn();
	});

}

/* gnb */
function gnb(){
	function menuOpenBtn() {
		$('.menuBtn').find('.srOnly').html('전체메뉴닫기');
		if (matchMedia("screen and (max-width: 640px)").matches) {
			TweenMax.to($('.hamburger .line1'), .2, {marginTop:0, width:28, height:3, rotation:-45, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line2'), .2, {opacity:0, width:0, height:3, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line3'), .2, {marginTop:0, width:28, height:3, rotation:45, ease:Circ.easeOut});
		} else {
			TweenMax.to($('.hamburger .line1'), .2, {marginTop:0, width:38, height:3, rotation:-45, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line2'), .2, {opacity:0, width:0, height:3, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line3'), .2, {marginTop:0, width:38, height:3, rotation:45, ease:Circ.easeOut});
		}
	};
	function menuCloseBtn() {
		$('.menuBtn').find('.srOnly').html('전체메뉴보기');
		if (matchMedia("screen and (max-width: 640px)").matches) {
			TweenMax.to($('.hamburger .line1'), .2, {marginTop:-9, width:26, height:2, rotation:0, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line2'), .2, {opacity:1, width:20, height:2, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line3'), .2, {marginTop:9, width:26, height:2, rotation:0, ease:Circ.easeOut});
		} else {
			TweenMax.to($('.hamburger .line1'), .2, {marginTop:-12, width:36, height:2, rotation:0, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line2'), .2, {opacity:1, width:28, height:2, ease:Circ.easeOut});
			TweenMax.to($('.hamburger .line3'), .2, {marginTop:12, width:36, height:2, rotation:0, ease:Circ.easeOut});
		}
	};
	function gnbOpen() {
		$('#header').addClass('gnbOpen');
		var gnbH = $('#gnb > ul').height() + 50;
		TweenMax.to($('#gnb'), .3, {height:gnbH, ease:Circ.easeOut});
		if($('#header').is(".mobileWrap")){
			TweenMax.to($('#gnb'), .3, {height:'auto', ease:Circ.easeOut});
			TweenMax.to($('.modalOverlay.gnbCover'), .3, {opacity:.5, display:"block", ease:Expo.easeOut});
		}
	};
	function gnbClose() {
		$('#header').removeClass('gnbOpen');
		if (matchMedia("screen and (max-width: 640px)").matches) {
			$("#gnb").css("height", "70px");
		} else if (matchMedia("screen and (max-width: 1024px)").matches) {
			$("#gnb").css("height", "100px");
		} else {
			TweenMax.to($('#gnb'), .3, {height:112, ease:Circ.easeOut});
		}
		if($('#header').is(".mobileWrap")){
			TweenMax.to($('.modalOverlay.gnbCover'), .3, {opacity:0, display:"none", ease:Expo.easeOut});
		}
	};
	function allMenuOpen() {
		$("html").css("overflow-y", "hidden");
		$('#header').css("overflow", "visible");
		$('#allMenu').show();
		TweenMax.to($('.modalOverlay.allMenuCover'), .4, {opacity:1, display:"block", ease:Expo.easeOut});
	};
	function allMenuClose() {
		$("html").css("overflow-y", "auto");
		$('#header').removeAttr('style');
		$('#allMenu').hide();
		TweenMax.to($('.modalOverlay.allMenuCover'), .4, {opacity:0, display:"none", ease:Expo.easeOut});
	};

	$('#gnb .depth1 > li').prepend("<span class='gnbMobileTouch'></span>");
	
	$('.allMenuBtn').on('click', function(e) {
		$('.allMenuBtn').toggleClass('close');

		if($('#header').is(".mobileWrap")){
			if($(this).is(".close")){
				$("html").css("overflow-y", "hidden");
				menuOpenBtn();
				gnbOpen();
			} else {
				$("html").css("overflow-y", "auto");
				menuCloseBtn();
				gnbClose();
			}
		} else {
			if($('#header').is(".gnbOpen")){
				$('#header').removeClass('gnbOpen');
			}
			if($(this).is(".close")){
				menuOpenBtn();
				allMenuOpen();
			} else {
				menuCloseBtn();
				allMenuClose();
			}
		}
		e.preventDefault();
	});
	$('#gnb .depth1 > li > a').on('mouseenter focus',function(){
		if (!matchMedia("screen and (max-width: 1024px)").matches) {
			gnbOpen();
		}
	});
	$('#header').on('mouseleave',function(){
		if (!matchMedia("screen and (max-width: 1024px)").matches) {
			if(!$('.menuBtn').is(".close")){
				gnbClose();
			}
		}
	});
	$('#gnb .depth1 > li > .gnbMobileTouch').on('click',function(){
		if($('#header').is(".mobileWrap")){
			if($(this).parent().is(".isOpen")) {
				$(this).parent().removeClass('isOpen');
				$(this).parent().find('.depth2').slideUp();
			} else {
				$('#gnb .depth1 > li').removeClass('isOpen');
				$(this).parent().addClass('isOpen');
				$('#gnb .depth2').slideUp();
				$(this).parent().find('.depth2').slideDown();
			}
		}
	});

	$('.langBtn > a').on('click',function(){
		$('.langList').slideDown(200);
	});
	$('.langBtn').on('mouseleave',function(){
		$('.langList').slideUp(200);
	});
	
	$(window).on('load resize', function(){
		if (matchMedia("screen and (max-width: 1024px)").matches) {
			$('#header').addClass('mobileWrap');
			$('#gnb .depth2').hide();
		} else {
			$('#header').removeClass('mobileWrap');
			$('#gnb .depth1 > li').removeClass('isOpen');
			$('#gnb .depth2').show();
		}
		allMenuClose();
		menuCloseBtn();
		$('.allMenuBtn').removeClass('close');
		$('#header').removeClass('gnbOpen');
		$('#gnb').removeAttr('style');
		if($('#header').is(".mobileWrap")){
			TweenMax.to($('.modalOverlay.gnbCover'), .3, {opacity:0, display:"none", ease:Expo.easeOut});
		}
	});
}

/* main */
function main(){
	$(".mainVisual").each(function(){
		var $slideVisual = $(this).children(".visualSlide");
		$slideVisual.on('init',function  (event, slick) {
			setTimeout(function() {
				TweenMax.to($(".slideItem:eq(0) .txtBox h3"), 1.2, {opacity:1, marginLeft:0, ease:Expo.easeOut});
				TweenMax.to($(".slideItem:eq(0) .txtBox p"), 1.2, {delay:.1, opacity:1, marginLeft:0, ease:Expo.easeOut});
			},1000);
		});
		$slideVisual.on('afterChange',function  (event, slick, currentSlide) {
			TweenMax.to($(".slideItem .txtBox h3"), 1.2, {opacity:0, marginLeft:"25%", ease:Expo.easeOut});
			TweenMax.to($(".slideItem .txtBox p"), 1.2, {opacity:0, marginLeft:"25%", ease:Expo.easeOut});
			TweenMax.to($(".slideItem:eq("+currentSlide+") .txtBox h3"), 1.2, {opacity:1, marginLeft:0, ease:Expo.easeOut});
			TweenMax.to($(".slideItem:eq("+currentSlide+") .txtBox p"), 1.2, {delay:.1, opacity:1, marginLeft:0, ease:Expo.easeOut});
		});
		$slideVisual.slick({
			dots:true,
			arrows:false,
			slidesToShow:1,
			slidesToScroll:1,
			autoplay:true,
			fade:true,
			autoplaySpeed:5000,
			speed: 1000,
			pauseOnHover:false
		});
	});

	TweenMax.to($(".scrollGuide span"), 0.5, {bottom:37, ease:Expo.ease, repeat:-1, repeatDelay:0, yoyo:true});
	
	$(".mainTechnology").each(function(){
		var $slidePhoto = $(this).find(".photoSlide");
		$slidePhoto.slick({
			dots: false,
			arrows: true,
			slidesToShow: 1,
			speed: 600,
		});
	});
	$(".mainNews").each(function(){
		var $slideContent = $(this).find(".contentSlide");
		$slideContent.slick({
			dots: false,
			arrows: true,
			slidesToShow: 4,
			speed: 600,
			responsive: [
				{
					breakpoint: 961,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 641,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 361,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
		  ]
		});
	});

	var controller = new ScrollMagic.Controller();
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", offset: -200})
					.setClassToggle("#scrollTween1", "titTween")
					.addTo(controller);
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger2", offset: -200})
					.setClassToggle("#scrollTween2", "titTween")
					.addTo(controller);
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger3", offset: -200})
					.setClassToggle("#scrollTween3", "titTween")
					.addTo(controller);
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger4", offset: -200})
					.setClassToggle("#scrollTween4", "titTween")
					.addTo(controller);
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger5", offset: -200})
					.setClassToggle("#scrollTween5", "titTween")
					.addTo(controller);
		
}

/* matchHeight */
function match(){
	var WinWdith = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if(WinWdith > 960){
		$('.conBoxList').each(function() {
			$(this).children('li').matchHeight({
				//remove: true
			});
		});
	} else {
		$('.conBoxList').each(function() {
			$(this).children('li').matchHeight({
				remove: true
			});
		});
	}
}

/* network */
function network(){
	$('img[usemap]').rwdImageMaps();
	$('#Map area').click(function(e){
		e.preventDefault();
		var scrollTo = $($(this).attr('href')).offset().top - 120;
		 $('body,html').stop(true,false).animate({scrollTop: scrollTo}, 600);
	});
}

/* enquiry */
function enquirySel(){
	$('.enquiryCont a').on('click', function() {
		$('.enquiryCont .conbox').removeClass('isActive');
		$(this).parent().addClass('isActive');
	});

}
