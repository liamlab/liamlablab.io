$(function(){
	kediCommon();

 	popupSlide();
	evSlide();
	puSlide();
	kjepSlide();
	contSlide();

	kediContent();
});


/*--------------------------------------------------*/
/*  kediGnb
/*--------------------------------------------------*/
function kediCommon(){
	
	// variables
	$html = $('html');
	$body = $('body');
	$header = $('#header');
	$headerTop = $('.headerTop');
	$gnb = $('#gnb');
	$lnb = $('#lnb');
	$searchDim = $('.searchDim');
	$menuBtn = $('.menuBtn');
	$searchFormBtn = $('.searchFormBtn');
	$searchBar = $('.searchBar');
	$hamburger = $('.hamburger');
	$partnerLink = $('.partnerLink');
	$partnerOpen = $('.partnerOpen');
	$partnerClose = $('.partnerClose');
	$moveTopBtn = $('.btnTop');
	$quickLink = $('#quickLink');

	// menu button animation
	function menuOpenBtn() {
		$menuBtn.find('.srOnly').html('메뉴닫기');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:0, width:28, rotation:-45, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {opacity:0, width:0, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line3'), .2, {marginTop:0, width:28, rotation:45, ease:Circ.easeOut});
	};
	function menuCloseBtn() {
		$menuBtn.find('.srOnly').html('메뉴보기');
		TweenMax.to($hamburger.find('.line1'), .2, {marginTop:-9, width:22, rotation:0, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line2'), .2, {opacity:1, width:22, ease:Circ.easeOut});
		TweenMax.to($hamburger.find('.line3'), .2, {marginTop:9, width:22, rotation:0, ease:Circ.easeOut});
	};

	// menu button click motion
	$menuBtn.on('click', function() {
		var winH = $(window).height(),
				gnbH = winH - 83;
		$menuBtn.toggleClass('close');
		if($(this).is('.close')){
			$html.css('overflow-y', 'hidden');
			$gnb.css('height', gnbH).show();
			menuOpenBtn();
		} else {
			$html.removeAttr('style');
			$gnb.hide();
			menuCloseBtn();
		};
	});

	// search button click motion
	$searchFormBtn.on('click', function() {
		$searchFormBtn.toggleClass('isOpen');
		if($(this).is('.isOpen')){
			$(this).find('.srOnly').html('Close Search Form');
			$html.css('overflow-y', 'hidden');
			$searchBar.show();
			$searchDim.show();
		} else {
			$(this).find('.srOnly').html('Open Search Form');
			$html.removeAttr('style');
			$searchBar.hide();
			$searchDim.hide();
		};
	});

	// web gnb motion
	$gnb.delegate('.depth1 > li', 'mouseenter focusin mouseleave', function (event) {
		if($header.hasClass('gnbWeb')){
			switch (event.type) {
				case 'mouseenter' :
					$(this).find('.depth2').slideDown(200);
				break;
				case 'focusin' :
					$gnb.find('.depth2').hide();
					$(this).find('.depth2').show();
				break;
				case 'mouseleave' :
					$(this).find('.depth2').hide();
				break;
			}
		}
	});

	// web lnb motion
	$lnb.delegate('ul > li > a', 'click', function (event) {
		switch (event.type) {
			case 'click' :
				if(!$(this).parent().is(".isActive")){
					$(this).parent().toggleClass('isOpen');
					if($(this).parent().is(".isOpen")){
						$(this).next('ul').slideDown(200);
					} else {
						$(this).next('ul').slideUp(200);
					}
				}
				if($(this).next().is('ul')) {
					event.preventDefault();
				}
			break;
		}
	});
	
	// mobile gnb motion
	$gnb.delegate('.depth1 > li > a', 'click', function (event) {
		if($header.hasClass('gnbMobile')){
			switch (event.type) {
				case 'click' :
					if($(this).parent().is('.isOpen')) {
						$(this).parent().removeClass('isOpen');
						$(this).parent().find('.depth2').slideUp(200);
					} else {
						$gnb.find('.depth1 > li').removeClass('isOpen');
						$(this).parent().addClass('isOpen');
						$gnb.find('.depth2').slideUp();
						$(this).parent().find('.depth2').slideDown(200);
					};
					if($(this).next().is('.depth2')) {
						event.preventDefault();
					}
				break;
			}
		}
	});

	// footer partners link
	$partnerOpen.on('click', function(e) {
		$(this).parent().toggleClass('isOpen');
		if($(this).parent().is('.isOpen')){
			$(this).next('div').slideDown(200);
		} else {
			$(this).next('div').slideUp(200);
		}
		e.preventDefault();
	});
	$partnerClose.on('click', function() {
		$(this).parents('.partnerLink').removeClass('isOpen');
		$(this).parent().slideUp(200);
	});
	$partnerLink.on('mouseleave',function(){
		if($(this).is('isOpen')){
			$(this).removeClass('isOpen');
			$(this).find('div').slideUp(200);
		}
	});

	// quick link
	$moveTopBtn.click(function(){
		$html.animate({scrollTop: 0}, 400);
		$body.animate({scrollTop: 0}, 400);
		return false;
	});

	// scroll effect
	$(window).scroll(function(){
		var position = $(window).scrollTop()
		if ($('#container > .inner').offset().top-106 <= position) {
			$quickLink.css({'position':'fixed', 'top':156});
		} else {
			$quickLink.css({'position':'absolute', 'top':346});
		};
	});

	// load resize effect
	$(window).on('load resize', function(){
		if (matchMedia('screen and (max-width: 1024px)').matches) {
			$header.removeClass().addClass('gnbMobile');
		} else {
			$header.removeClass().addClass('gnbWeb');
		}
		$html.removeAttr('style');
		$gnb.removeAttr('style');
		$gnb.find('.depth1 > li').removeClass();
		$gnb.find('.depth2').removeAttr('style');
		$hamburger.find('i').removeAttr('style');
		$menuBtn.removeClass('close');
		$searchFormBtn.removeClass('isOpen');
		//$searchBar.hide();
		//$modalOverlay.hide();
	});
	
}


/*--------------------------------------------------*/
/*  popup Slide
/*--------------------------------------------------*/
function popupSlide() {

	var $popupSlide = $(".popupSlide");

	$popupSlide.on('init',function (event, slick, currentSlide) {
		$('.slick-dots').wrap('<div class="slideControl"></div>');
		$('.slideControl').append('<button class="slidePlay pause"><span class="srOnly">슬라이드 정지</span></button>');
	});

	$popupSlide.slick({
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		speed: 300,
		pauseOnHover: true
	});

	$('.slidePlay').on('click', function(e) {
		$(this).toggleClass('pause');
		if($(this).is(".pause")) {
			$(this).find('.srOnly').html('슬라이드 정지');
			$slideVisual.slick('slickPlay');
		} else {
			$(this).find('.srOnly').html('슬라이드 재생');
			$slideVisual.slick('slickPause');
		}
		e.preventDefault();
	});
	
}


/*--------------------------------------------------*/
/*  events Slide
/*--------------------------------------------------*/
function evSlide() {

	var $bdSlide = $(".evList"),
			$currentNum = $('.evControl .bdState .currentNum'),
			$totalNum = $('.evControl .bdState .totalNum'),
			$bdPrev = $('.evControl .bdPrev'),
			$bdNext = $('.evControl .bdNext');

	$bdSlide.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
		var i = (currentSlide ? currentSlide : 0) + 1;
		$currentNum.text(i);
		$totalNum.text(slick.slideCount);
	});

	$bdPrev.on('click', function() {
		$bdSlide.slick('slickPrev');
	});

	$bdNext.on('click', function() {
		$bdSlide.slick('slickNext');
	});

	$bdSlide.slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		speed: 300,
		pauseOnHover: true
	});

}


/*--------------------------------------------------*/
/*  publications Slide
/*--------------------------------------------------*/
function puSlide() {

	var $bdSlide = $(".puList"),
			$currentNum = $('.puControl .bdState .currentNum'),
			$totalNum = $('.puControl .bdState .totalNum'),
			$bdPrev = $('.puControl .bdPrev'),
			$bdNext = $('.puControl .bdNext');

	$bdSlide.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
		var i = (currentSlide ? currentSlide : 0) + 1;
		$currentNum.text(i);
		$totalNum.text(slick.slideCount);
	});

	$bdPrev.on('click', function() {
		$bdSlide.slick('slickPrev');
	});

	$bdNext.on('click', function() {
		$bdSlide.slick('slickNext');
	});

	$bdSlide.slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		speed: 300,
		pauseOnHover: true
	});
	
}


/*--------------------------------------------------*/
/*  kjep Slide
/*--------------------------------------------------*/
function kjepSlide() {

	var $kjepSlide = $(".kjepSlide");

	$kjepSlide.slick({
		arrows: false,
		dots: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		speed: 300,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	
}


/*--------------------------------------------------*/
/*  content Slide
/*--------------------------------------------------*/
function contSlide() {

	var $slideCont = $(".contSlide");

	$slideCont.slick({
		dots: false,
		arrows: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 5000,
		speed: 300,
		responsive: [
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

}


/*--------------------------------------------------*/
/*  kedi Content
/*--------------------------------------------------*/
function kediContent() {

	// matchHeight
	$(window).on('load resize', function(){
		var matchItem = $('.cardList ul, .sitemapUnit ul, .gridList, .EduOfficeList, statisticsList');
		matchItem.each(function() {
			$(this).children('li').matchHeight();
		});
		$('.divBox > h5').matchHeight();
		$('.divBox > ul > li:nth-child(1)').matchHeight();
		$('.divBox > ul > li:nth-child(2)').matchHeight();
		$('.divBox > ul > li:nth-child(3)').matchHeight();
		$('.divBox > ul > li:nth-child(4)').matchHeight();
		$('.divBox > ul > li:nth-child(5)').matchHeight();
	});

	// acknowledge open/close
	$('.acknOpen').on('click', function() {
		$('.acknCont').show();
	});
	$('.acknClose').on('click', function() {
		$('.acknCont').hide();
	});

	// layer open/close
	$(".viewProfile").click(function () {
		var getPopupID = $(this).attr("name");
		$('#'+getPopupID).show();
		$('.layerDim').show();
		$html.css('overflow-y', 'hidden');
	});
	$('.popupClose, .layerDim').click(function() {
		$('.layerWrap').hide();
		$('.layerDim').hide();
		$html.removeAttr('style');
	})

	// tab 
	$('.tabBtn').on('click', function() {
		$(this).parents('.tabWrap').toggleClass('isOpen');
	});
	$('.tabList a').on('click', function() {
		var isActiveText = $(this).text();
		$('.tabList li').removeClass('isActive');
		$(this).parent().addClass('isActive');
		$(this).parents('.tabWrap').removeClass('isOpen');
		$('.tabBtn').html(isActiveText);
	});

	// accordionList 
	$('.viewBtn').on('click', function() {
		if($(this).parents('.accordionList').is('.isOpen')){
			$(this).parents('.accordionList').removeClass('isOpen');
			$(this).find('.srOnly').html('Contents Show');
		} else {
			$(this).parents('.accordionList').addClass('isOpen');
			$(this).find('.srOnly').html('Contents Hide');
		};
	});

	// detailList Height 20191224
	$('.detailList>dl>dt>span').each( function() {
		var dlH = $(this).height(),
				ddH = dlH+25;
		if (dlH>25) {
			$(this).parent().next().css('min-height',ddH);
		}
	});
	
}